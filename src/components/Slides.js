import React, { Component } from 'react';

export default class Slides extends Component {
  render() {
    const {
      prevSlide,
      nextSlide,
      resetSlides,
      slides,
      index,
    } = this.props;

    const {title, text} = slides[index];

    return (
      <div className="App">
        <div id="navigation">
          <button id="button-restart" data-testid="button-restart" disabled={index===0} onClick={resetSlides}>Restart</button>
          <button id="button-prev" data-testid="button-prev" disabled={index<=0} onClick={prevSlide}>Prev</button>
          <button id="button-next" data-testid="button-next" disabled={index >= slides.length-1} onClick={nextSlide}>Next</button>
        </div>

        <div id="slide">
          <h1 id="title" data-testid="title">{title}</h1>
          <p id="text" data-testid="text">{text}</p>
        </div>
      </div>
    );
  }
}
